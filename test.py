import sys
import bs4 as bs
from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEnginePage
import time


from PyQt5.QtWidgets import QApplication, QWidget, QLabel


class Page(QWebEnginePage):
    def __init__(self, url):
        self.app = QApplication(sys.argv)
        QWebEnginePage.__init__(self)
        self.html = ''
        self.loadFinished.connect(self._on_load_finished)
        self.load(QUrl(url))
        time.sleep(2)
        self.app.exec_()

    def _on_load_finished(self):
        self.html = self.toHtml(self.Callable)

    def Callable(self, html_str):
        self.html = html_str
        self.app.quit()

def main():
    page = Page("https://weerindelft.nl/WU/55ajax-dashboard-testpage.php")
    soup = bs.BeautifulSoup(page.html, 'html.parser')
    js_test = soup.select('#ajaxtemp')[0].getText().strip()
    print(js_test)
    app1 = QApplication(sys.argv)
    w = QWidget()
    w.setGeometry(300, 300, 250, 150)  # x, y, w, h
    w.setWindowTitle("Temperature Display")
    label = QLabel(js_test, w)
    label.setToolTip("This is a <b>QLabel</b> widget with Tooltip")
    label.resize(label.sizeHint())
    label.move(80, 50)
    w.show()
    app1.exec_()
    app1.quit()


if __name__ == '__main__':
    main()
